package samplepackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

// This is a sample java main  class
/*
 * This class creates a webdriver object and invokes google website and searches for cpsat keyword
 * First program to get going in selenium
 */

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {
	

	public static void main(String[] args) {

		WebDriver driver;
		
		
		// please ensure that proper chromedriver is there in the resource\driver folder
		// if it is not there it may give an exception
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		
		driver = new ChromeDriver();	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		

		// locate the search box on the google page
		
		By bySearchBox = By.name("q");
		
		// create a webelement using this locator
		
		WebElement elementSearchBox = driver.findElement(bySearchBox);
		
		//clear the search box if there is anything in there
		elementSearchBox.clear();
		
		String searchString = "CPSAT";

		// send the search string CPSAT in the google search text box using sendkeys
		elementSearchBox.sendKeys(searchString);
		
		//Send Enter Key to the search Box
		elementSearchBox.sendKeys(Keys.ENTER);
		
		// hard coded wait for 1 second using thread.sleep
		// 1000 ms = 1 second
		// this is to just wait for the search page to load
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//close the webdriver
		driver.quit();
	}

}
